package main.com.example.request;


import main.com.example.Quote.QuoteRequest;
import main.com.example.marketmaker.ReferencePriceSource;
import main.com.example.response.ResponseResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
//import static org.junit.Assert.;

/**
 * Created by tony kang on 1/12/2019.
 */
public class QuoteCalculatorImplTest {
    private static final int securtiyId = 123;

    private QuoteCalculator quoteCalculator;

    @BeforeClass
    public void setUp() {
        quoteCalculator = new QuoteCalculatorImpl();
    }

    @AfterClass
    public void tearDown() {
        quoteCalculator = null;
    }

    // should be able to mock the service and assert the result with Mockito imported
    // Here we skip it as the env setup has some issue
    @Test
    public void testCalculateQuotePrice() {
        /*ReferencePriceSource referencePriceSource = Mockito.mock(ReferencePriceSource.class);
        when(referencePriceSource.get(eq(securtiyId))).thenReturn(25.3d);
        QuoteRequest testQuote = new QuoteRequest("123 BUY 100");

        ResponseResult responseResult = quoteCalculator.calculateQuotePrice(testQuote, referencePriceSource);
        assertEquals(2530, responseResult.getQuotePrice());
        */
    }
}