package main.com.example.request;

import main.com.example.Quote.QuoteRequest;
import main.com.example.marketmaker.ReferencePriceSource;
import main.com.example.response.ResponseResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.PrintWriter;

import static org.testng.AssertJUnit.assertEquals;

/**
 * Created by kairui on 1/12/2019.
 */
class QuoteCoordinatorTest {

    private static final int securtiyId = 123;

    private QuoteCoordinator quoteCoordinator;

    @BeforeClass
    public void setUp() {
        quoteCoordinator = new QuoteCoordinator();
    }

    @AfterClass
    public void tearDown() {
        quoteCoordinator = null;
    }

    // should be able to mock the service and assert the result with Mockito imported
    // Here we skip it as the env setup has some issue
    @Test
    public void testAcceptQuote() {
        /*ArgumentCap<PrintWriter> argumentCap = ArgumentCaptor.forClass(PrintWriter.class);
        PrintWriter responseHandler = Mockito.mock(PrintWriter.class);

        quoteCoordinator.accept("123 BUY 100", responseHandler);
        verify(responseHandler, times(1)).println(argumentCap.capture());
        assertEquals(2530, argumentCap.getValue());
        */
    }
}