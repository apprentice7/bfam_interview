package main.com.example.request;


import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertTrue;


/**
 * Created by tony kang on 1/12/2019.
 */

public class RequestValidatorTest {

    @Test
    public void testValidator() {
        RequestValidator requestValidator = RequestValidator.getRequestValidator();
        assertTrue(requestValidator.validate("123 BUY 100"));
        assertTrue(requestValidator.validate("12345 BUY 1000000"));
        assertTrue(requestValidator.validate("123 SELL 10"));

        assertFalse(requestValidator.validate("123 Buy 10"));
        assertFalse(requestValidator.validate("123 sell 10"));
        assertFalse(requestValidator.validate("123 BUY -10"));
        assertFalse(requestValidator.validate("123 BUY 10 100"));
        assertFalse(requestValidator.validate("123 BUY"));
    }
}