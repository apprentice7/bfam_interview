package main.com.example.Quote;

/**
 * Created by tony kang on 30/11/2019.
 */
public class QuoteRequest {

    public enum Side {
        BUY,
        SELL
    }

    private int securityId;
    private Side side;
    private int quantity;

    public QuoteRequest(String quoteRequest) {
        String[] quoteComponents = quoteRequest.split(" ");
        this.securityId = Integer.valueOf(quoteComponents[0]);
        this.side = quoteComponents[1].equals("BUY") ? Side.BUY : Side.SELL;
        this.quantity = Integer.valueOf(quoteComponents[2]);
    }

    public int getSecurityId() {
        return securityId;
    }

    public Side getSide() {
        return side;
    }

    public int getQuantity() {
        return quantity;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("securityId: ").append(securityId).append("\n");
        sb.append("side: ").append(side.name()).append("\n");
        sb.append("quantity: ").append(quantity);
        return sb.toString();
    }
}
