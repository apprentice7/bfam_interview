package main.com.example.request;

import main.com.example.Quote.QuoteRequest;
import main.com.example.marketmaker.ReferencePriceSource;
import main.com.example.response.ResponseResult;

/**
 * Created by tony kang on 1/12/2019.
 */
public interface QuoteCalculator {

    /**
     * Calculator accept a valid quote and reference price source to calculate the response result for the quote request
     * @param quote parsed quote object with securityId, Side and quantity information
     * @param refPriceSource reference price source that provides the reference price
     * @return response price result
     */
    ResponseResult calculateQuotePrice(QuoteRequest quote, ReferencePriceSource refPriceSource);
}
