package main.com.example.request;

import main.com.example.Quote.QuoteRequest;
import main.com.example.marketmaker.ReferencePriceSource;
import main.com.example.marketmaker.ReferencePriceSourceListener;
import main.com.example.response.ResponseResult;

import java.io.PrintWriter;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.logging.Logger;

import static main.com.example.response.ResponseResult.FAILURE_RESULT;

/**
 * Created by tony kang on 1/12/2019.
 */
public class QuoteCoordinator {
    private static final Logger log = Logger.getLogger(QuoteCoordinator.class.getName());

    private ConcurrentMap<Integer, ReferencePriceSource> refPriceMap = new ConcurrentHashMap<>();
    private RequestValidator requestValidator = RequestValidator.getRequestValidator();
    private QuoteCalculator quoteCalculator = new QuoteCalculatorImpl();

    public void accept(String quoteRequest, PrintWriter responseHandler) {
        if(!requestValidator.validate(quoteRequest)) {
            log.severe("Invalid request: " + quoteRequest);
            respond(responseHandler, FAILURE_RESULT);
        }

        QuoteRequest quote = new QuoteRequest(quoteRequest);
        ReferencePriceSource refSource = getReferencePriceSource(quote);
        ResponseResult responseResult = quoteCalculator.calculateQuotePrice(quote, refSource);

        respond(responseHandler, responseResult);
    }

    private void respond(PrintWriter responseHandler, ResponseResult result) {
        responseHandler.println(result.getQuotePrice());
        responseHandler.flush();
    }

    private ReferencePriceSource getReferencePriceSource(QuoteRequest quote) {
        return refPriceMap.computeIfPresent(quote.getSecurityId(), (k, v) -> {
            ReferencePriceSource source = new ReferencePriceSource() {
                @Override
                public void subscribe(ReferencePriceSourceListener listener) {

                }

                @Override
                public double get(int securityId) {
                    return 0;
                }
            };
            return source;
        });
    }
}
