package main.com.example.request;

import main.com.example.Quote.QuoteRequest;
import main.com.example.marketmaker.QuoteCalculationEngine;
import main.com.example.marketmaker.ReferencePriceSource;
import main.com.example.response.ResponseResult;

/**
 * Created by tony kang on 1/12/2019.
 */
public class QuoteCalculatorImpl implements QuoteCalculator {

    private QuoteCalculationEngine quoteCalculationEngine;

    @Override
    public ResponseResult calculateQuotePrice(QuoteRequest quote, ReferencePriceSource refPriceSource) {
        double refPrice = refPriceSource.get(quote.getSecurityId());
        double quotePrice = quoteCalculationEngine.calculateQuotePrice(quote.getSecurityId(), refPrice,
                quote.getSide() == QuoteRequest.Side.BUY, quote.getQuantity());
        return new ResponseResult(quotePrice);
    }
}
