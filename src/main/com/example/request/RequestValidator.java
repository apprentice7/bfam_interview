package main.com.example.request;

/**
 * Created by tony kang on 30/11/2019.
 */
public class RequestValidator {

    private static RequestValidator requestValidator = new RequestValidator();
    private static final String BUY = "BUY";
    private static final String SELL = "SELL";

    // Ideally should be created as a spring bean object
    public static RequestValidator getRequestValidator() {
        return requestValidator == null ? new RequestValidator() : requestValidator;
    }

    private RequestValidator() {}

    boolean validate(String request) {
        String[] requestComponent = request.split(" ");
        if(requestComponent.length != 3) return false;
        if(!BUY.equals(requestComponent[1]) && !SELL.equals(requestComponent[1])) return false;
        if(Integer.valueOf(requestComponent[2]) < 0) return false;

        // some other criteria should apply here to validate the request
        // here for simplicity we ignore it.
        return true;
    }
}
