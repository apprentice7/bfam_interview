package main.com.example.request;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Logger;

/**
 * Created by tony kang on 30/11/2019.
 */
public class RequestHandler extends Thread {
    private static final Logger log = Logger.getLogger(RequestHandler.class.getName());

    private Socket socket;
    private QuoteCoordinator quoteCoordinator = new QuoteCoordinator();

    public RequestHandler(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            BufferedReader inputReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter responseHandler = new PrintWriter(socket.getOutputStream());
            String quoteRequest = inputReader.readLine();

            while(quoteRequest != null && quoteRequest.length() > 0) {
                log.info("Received quote request: " + quoteRequest);
                quoteCoordinator.accept(quoteRequest, responseHandler);
                quoteRequest = inputReader.readLine();
            }
            log.info("Finished quote request.");

            inputReader.close();
            responseHandler.close();
            socket.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

}
