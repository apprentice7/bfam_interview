package main.com.example.response;

/**
 * Created by tony kang on 30/11/2019.
 */
public class ResponseResult {
    public static final ResponseResult FAILURE_RESULT = new ResponseResult(-1d);

    private double quotePrice;

    public ResponseResult(double quotePrice) {
        this.quotePrice = quotePrice;
    }

    public double getQuotePrice() {
        return quotePrice;
    }

    public void setQuotePrice(double quotePrice) {
        this.quotePrice = quotePrice;
    }
}
