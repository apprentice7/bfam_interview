package main.com.example.server;

import main.com.example.request.RequestHandler;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Logger;

/**
 * Created by tony kang on 30/11/2019.
 */
public class SocketServer extends Thread {
    private static final Logger log = Logger.getLogger(SocketServer.class.getName());

    private ServerSocket serverSocket;
    private int port;
    private boolean running = false;

    public SocketServer(int port) {
        this.port = port;
    }

    public void startSocketServer() {
        try {
            serverSocket = new ServerSocket(port);
            this.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void stopSocketServer() {
        running = false;
        this.interrupt();
    }

    @Override
    public void run() {
        running = true;
        while(running) {
            try {
                log.info("Socket server starting.");
                Socket socket = serverSocket.accept();
                RequestHandler requestHandler = new RequestHandler(socket);
                requestHandler.start();
                log.info("Socket server started.");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
